const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth")

// Create course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

// Retrieve all Courses
router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Get All Active
router.get("/", (req,res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieve a specific course
router.get("/:courseId", (req,res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
router.put("/:courseId", auth.verify, (req,res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY
// ARCHIVE Course
// solution: get all active
router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// retrieve a specific course
router.get("/:courseId", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	
	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
	
});



// export the router object for index.js file
module.exports = router;
